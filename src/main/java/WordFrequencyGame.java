import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String SPACE_PATTERN = "\\s+";

    public String calculateWordFrequency(String sentence) {
        try {
            String[] words = sentence.split(SPACE_PATTERN);
            List<WordFrequency> wordFrequencyList = getWordFrequencies(words);
            wordFrequencyList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
            StringJoiner joiner = createWordFrequencyStringJoiner(wordFrequencyList);
            return joiner.toString();
        } catch (Exception exception) {
            return "Calculate Error";
        }
    }

    private static StringJoiner createWordFrequencyStringJoiner(List<WordFrequency> wordFrequencyList) {
        StringJoiner joiner = new StringJoiner("\n");
        wordFrequencyList.stream().forEach(word -> {
            joiner.add(word.getValue() + " " + word.getWordCount());
        });
        return joiner;
    }

    private List<WordFrequency> getWordFrequencies(String[] words) {
        HashSet<String> deduplicate = new HashSet<>(List.of(words));
        return deduplicate.stream().map(word -> new WordFrequency(word, Collections.frequency(List.of(words), word)))
                .collect(Collectors.toList());
    }
}
